# REAL-TIME FACE MASK DETECTION WEB APPLICATION

## Introduction
This is the source code of the student research project **Real-time Face Mask Detection Software** at [International University - VNU-HCM](https://hcmiu.edu.vn/en/) by:
<ul>
<li> Phạm Hoàng Nam Anh - Principal investigator </li>
<li> Phùng Khánh Linh - Member </li>
<li> Lê Thị Tường Vi - Member </li>
 and under the supervision of Academic advisor Dr. Lý Tú Nga 
</ul>
## Running Instruction
- Install required packages in `requirements.txt`
- Set video source and model weight used in `facemask_server.py`.
- Open CMD in root folder, set the export file: `set FLASK_APP=facemask_server`
- Run Flask `flask run`
- The web app is running on `http://localhost:5000/video`


